# AWS Batch automation resources

Resources for [AWS Batch](https://aws.amazon.com/batch/) service: CloudFormation Template and Compute Environment with LaunchTemplate examples to add more storage and optimize the ECS-agent configuration on the Batch compute hosts.

### Tutorial

This is a tutorial to change the configuration of the ecs/ec2 instance to use in [AWS Batch](https://aws.amazon.com/batch/) by adding more storage and optimizing the ECS-agent configuration. These settings are suitable for a HPC environment. This tutorial provides guidance for using the [AWS Batch](https://aws.amazon.com/batch/) Managed Compute Environment with: 

- option 1: a Custom ECS-Optimized AMI running Amazon Linux2 using Spot
- option 2: the AWS Batch Default ECS-Optimized AMI using OnDemand

This manual asumes there is already a VPC with correctly configured Subnets to place the instances along with a Security Group, IAM roles, configured awscli a KMS Key (optionaly) and an SSH Keypair. This is targeted for the N-Virginia region us-east-1. 

*If you need a VPC and the required resources you can use the [aws-batch-vpc.yaml](https://gitlab.com/henrybravo/aws-batch/-/blob/master/aws-batch-vpc.yaml) CloudFormation template included in this repository. You would still need to create a KMS Key to encrypt the data drive or you can disable encryption in the LaunchTemplate.*

#### To get started clone this repository

```bash
$ git clone https://gitlab.com/henrybravo/aws-batch.git
```

### option 1 - cloudformation: deploy a compute environment and a launch template with cloudformation based on Amazon Linux 2

This a CloudFormation Template to create a Custom **Managed** AMI on Amazon Linux 2 (AMI id is hardcoded in template) in the N-Virginia us-east-1 region. It allocates additional encrypted storage for docker on the Overlay2 storage driver. And finally it configures the ECS-agent  with additional custom settings (see [userdata section here](https://gitlab.com/henrybravo/aws-batch/-/tree/master#create-the-batch-service-and-the-requirements)). It asumes there is already a VPC with correctly configured Subnets to place the instances along with a KMS Key for the encrypted disk, Security Group, awscli and SSH keypair.

The CloudFormation Template deploys the following resources:
- ServiceRole
- InstanceProfile
- InstanceRole
- LaunchTemplate
- ComputeEnvironment
- JobDefinition example
- JobQueue

#### To deploy this template with the defaults and mandatory parameters:

```bash
aws cloudformation create-stack \
  --region us-east-1 \
  --stack-name aws-batch-custom-ami \
  --template-body file://aws-batch-custom-ami.yaml \
  --capabilities CAPABILITY_NAMED_IAM
  --parameters \
    ParameterKey=KmsKeyArn,ParameterValue='arn:aws:kms:us-east-1:xxxx:key/xxxx' \
    ParameterKey=SSHKeyPairName,ParameterValue='xxxx' \
    ParameterKey=Subnets,ParameterValue='subnet-xxxx\,subnet-xxxx' \
    ParameterKey=SecurityGroup,ParameterValue='sg-xxxx' \
```

### option 2 - manual: deploy a compute environment and a launch template based on Amazon Linux

This section explains how to deploy a a compute environment and a launch template using the awscli and the files in this repo: [compute environment](https://gitlab.com/henrybravo/aws-batch/-/blob/master/aws-batch-custom-ami.json) + [launch template](https://gitlab.com/henrybravo/aws-batch/-/blob/master/aws-batch-custom-ami-launch-template.json). We will use the default AWS Batch Managed AMI and we will add aditional configuration on top of it.

#### Create the batch service and the requirements

1.	Ensure VPC, Subnets, SG, IAM roles, awscli and SSH Keypair exist and are configured

2.	Create EC2 Launch Template

The Launch Template adds a 300GB additional storage disk and optimized ecs-agent configuration in the userdata (see [userdata file](https://gitlab.com/henrybravo/aws-batch/-/blob/master/userdata) for all specs )

Launch Template file:

```bash
{
  "LaunchTemplateName": "batch-optm-storage-and-ecsconfig",
    "LaunchTemplateData": {
      "BlockDeviceMappings": [
          {
              "DeviceName": "/dev/xvdcz",
              "Ebs": {
                  "Encrypted": false,
                  "VolumeSize": 300,
                  "VolumeType": "gp2"
              }
          }
      ],
      "Monitoring": {
          "Enabled": true
      },
      "UserData": "TUlNRS1WZXJzaW9uOiAxLjAKQ29udGVudC1UeXBlOiBtdWx0aXBhcnQvbWl4ZWQ7IGJvdW5kYXJ5PSI9PUJPVU5EQVJZPT0iCgotLT09Qk9VTkRBUlk9PQpDb250ZW50LVR5cGU6IHRleHQveC1zaGVsbHNjcmlwdDsgY2hhcnNldD0idXMtYXNjaWkiCgojIFdyaXRlIEVDUyBjb25maWcgZmlsZQpjYXQgPDwgRU9GID4gL2V0Yy9lY3MvZWNzLmNvbmZpZwpFQ1NfRU5HSU5FX1RBU0tfQ0xFQU5VUF9XQUlUX0RVUkFUSU9OPTFtCkVDU19DT05UQUlORVJfU1RPUF9USU1FT1VUPTVzCkVDU19DT05UQUlORVJfU1RBUlRfVElNRU9VVD01cwpFQ1NfRElTQUJMRV9JTUFHRV9DTEVBTlVQPWZhbHNlCkVDU19JTUFHRV9DTEVBTlVQX0lOVEVSVkFMPTEwbQpFQ1NfSU1BR0VfTUlOSU1VTV9DTEVBTlVQX0FHRT0xMG0KTk9OX0VDU19JTUFHRV9NSU5JTVVNX0NMRUFOVVBfQUdFPTEwbQpFQ1NfTlVNX0lNQUdFU19ERUxFVEVfUEVSX0NZQ0xFPTUwCkVDU19FTkFCTEVfVU5UUkFDS0VEX0lNQUdFX0NMRUFOVVA9dHJ1ZQpFT0YKCi0tPT1CT1VOREFSWT09LS0K",
      "TagSpecifications": [
          {
              "ResourceType": "instance",
              "Tags": [
                  {
                      "Key": "Name",
                      "Value": "custom-batch-processing"
                  }
              ]
          }
      ]
  }
}
```
ECS-agent configuration entries passed from the EC2 UserData file, optimized for workload with hundred thousands to millions of running jobs:

```bash
ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION=1m # Time to wait from when a task is stopped until the Docker container is removed

ECS_CONTAINER_STOP_TIMEOUT=5s # Time to wait from when a task is stopped before its containers are forcefully stopped if they do not exit normally on their own

ECS_CONTAINER_START_TIMEOUT=5s # Time to wait before giving up on starting a container

ECS_DISABLE_IMAGE_CLEANUP=false # Whether to disable automated image cleanup for the Amazon ECS agent

ECS_IMAGE_CLEANUP_INTERVAL=10m # The time interval between automated image cleanup cycles. If set to less than 10 minutes, the value is ignored.

ECS_IMAGE_MINIMUM_CLEANUP_AGE=10m # This would be in case of having other images in the lvm thinpool, not essential indeed

NON_ECS_IMAGE_MINIMUM_CLEANUP_AGE=10m # The minimum time interval between when an image is pulled and when it can be considered for automated image cleanup

ECS_NUM_IMAGES_DELETE_PER_CYCLE=50 # have a high number in case of many images

ECS_ENABLE_UNTRACKED_IMAGE_CLEANUP=true # whether to allow the Amazon ECS agent to delete containers and images that are not part of Amazon ECS tasks
```

To deploy the Launch Template:

```bash 
$ aws ec2 --region us-east-1 create-launch-template --cli-input-json file://aws-batch-custom-ami-launch-template.json
```

The output of this command should be something similar to this:

```bash
{
    "LaunchTemplate": {
        "LaunchTemplateId": "lt-1234567890abcdefg",
        "LaunchTemplateName": "batch-optmz-storage-ecsconfig",
        "CreateTime": "2020-03-09T16:46:10.000Z",
        "CreatedBy": "arn:aws:iam::xxxxxxx:user/xxxxxxxx",
        "DefaultVersionNumber": 1,
        "LatestVersionNumber": 1
    }
}
```

We need the LaunchTemplateId in our aws batch create-compute-environment next.

3.	Create AWS Batch compute environment

```bash 
$ aws batch --region us-east-1 create-compute-environment --cli-input-json file://aws-batch-custom-ami.json
```

4.	Using the AWS WebConsole: Create job queues and attach then to the created compute environment 'aws-batch-custom-ami'

5.	Using the AWS WebConsole: Create job definitions

6.	Using the AWS WebConsole: Create jobs
